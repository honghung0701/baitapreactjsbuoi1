import logo from './logo.svg';
import './App.css';
import Header from './BaiTapThucHanhLayout/Header';
import Body from './BaiTapThucHanhLayout/Body';
import Item from './BaiTapThucHanhLayout/Item';
import Footer from './BaiTapThucHanhLayout/Footer';
import Banner from './BaiTapThucHanhLayout/Banner';

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
